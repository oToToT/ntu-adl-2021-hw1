#!/bin/bash

# slot related files

mkdir -p slot-models

if [ ! -f slot-models/770.pt ]
then
  wget https://adl-models.ototot.tk/slot/770.pt -O slot-models/770.pt
fi

if [ ! -f slot-models/771.pt ]
then
  wget https://adl-models.ototot.tk/slot/771.pt -O slot-models/771.pt
fi

if [ ! -f slot-models/vocab.pkl ]
then
  wget https://adl-models.ototot.tk/slot/vocab.pkl -O slot-models/vocab.pkl
fi

if [ ! -f slot-models/tag2idx.json ]
then
  wget https://adl-models.ototot.tk/slot/tag2idx.json -O slot-models/tag2idx.json
fi

if [ ! -f slot-models/embeddings.pt ]
then
  wget https://adl-models.ototot.tk/slot/embeddings.pt -O slot-models/embeddings.pt
fi

# intent related files

mkdir -p intent-models

if [ ! -f intent-models/2791.pt ]
then
  wget https://adl-models.ototot.tk/intent/2791.pt -O intent-models/2791.pt
fi

if [ ! -f intent-models/vocab.pkl ]
then
  wget https://adl-models.ototot.tk/intent/vocab.pkl -O intent-models/vocab.pkl
fi

if [ ! -f intent-models/intent2idx.json ]
then
  wget https://adl-models.ototot.tk/intent/intent2idx.json -O intent-models/intent2idx.json
fi

if [ ! -f intent-models/embeddings.pt ]
then
  wget https://adl-models.ototot.tk/intent/embeddings.pt -O intent-models/embeddings.pt
fi
