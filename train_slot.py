import json
import pickle
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Dict

import torch
from tqdm import trange, tqdm

from slot_model import SlotTagger
from dataset import SeqTagDataset
from utils import Vocab

TRAIN = "train"
DEV = "eval"
SPLITS = [TRAIN, DEV]


def main1(args: Namespace):
    with open(args.cache_dir / "vocab.pkl", "rb") as vocab_file:
        vocab: Vocab = pickle.load(vocab_file)

    intent_idx_path = args.cache_dir / "tag2idx.json"
    intent2idx: Dict[str, int] = json.loads(intent_idx_path.read_text())

    data_paths = {split: args.data_dir / f"{split}.json" for split in SPLITS}
    data = {
        split: json.loads(path.read_text())
        for split, path in data_paths.items()
    }
    datasets: Dict[str, SeqTagDataset] = {
        split: SeqTagDataset(split_data, vocab, intent2idx, args.max_len)
        for split, split_data in data.items()
    }
    data_loaders = {
        split: torch.utils.data.DataLoader(dataset=dataset,
                                           batch_size=args.batch_size,
                                           shuffle=True,
                                           collate_fn=dataset.collate_fn,
                                           num_workers=args.load_workers)
        for split, dataset in datasets.items()
    }

    embeddings = torch.load(args.cache_dir / "embeddings.pt")
    model = SlotTagger(embeddings, args.hidden_size, args.num_layers,
                       args.dropout, args.bidirectional,
                       datasets[TRAIN].num_classes,
                       args.max_len).to(args.device)

    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
    loss_weights = torch.ones(datasets[TRAIN].num_classes,
        dtype=torch.float,
        device=args.device)
    loss_weights[datasets[TRAIN].label2idx('O')] = 0.3
    loss_weights[datasets[TRAIN].label2idx('pad')] = 0.01
    criterion = torch.nn.CrossEntropyLoss(loss_weights)
    # criterion = torch.nn.CrossEntropyLoss()

    epoch_pbar = trange(args.num_epoch, desc="Epoch")
    best_acc: float = 0
    for epoch in epoch_pbar:
        if best_acc >= 770:
            break
        model.train()
        train_loss: float = 0
        train_acc: float = 0
        for batch in data_loaders[TRAIN]:
            texts = torch.tensor(batch['tokens'], device=args.device)
            intents = torch.tensor(batch['tags'], device=args.device)

            optimizer.zero_grad()
            result = model(texts)
            loss = criterion(result.reshape(-1, datasets[TRAIN].num_classes), intents.reshape(-1))

            loss.backward()
            optimizer.step()

            train_acc += torch.sum(torch.all(torch.eq(result.argmax(2), intents),
                                             dim=1)).item()
            train_loss += loss.item()
        tqdm.write(
            f'[{epoch: 3d} / {args.num_epoch}] - train_loss: {train_loss: .5f}, train_acc: {train_acc / len(datasets[TRAIN]): .5f}'
        )

        eval_loss: float = 0
        eval_acc: float = 0
        model.eval()
        with torch.no_grad():
            for batch in data_loaders[DEV]:
                texts = torch.tensor(batch['tokens'], device=args.device)
                intents = torch.tensor(batch['tags'], device=args.device)

                result = model(texts)
                loss = criterion(result.reshape(-1, datasets[TRAIN].num_classes), intents.reshape(-1))

                eval_acc += torch.sum(
                    torch.all(torch.eq(result.argmax(2), intents), dim=1)).item()

                eval_loss += loss.item()
            tqdm.write(
                f'[{epoch: 3d} / {args.num_epoch}] - valid_loss: {eval_loss: .5f}, valid_acc: {eval_acc / len(datasets[DEV]): .5f}'
            )
            if eval_acc > best_acc:
                best_acc = eval_acc
                torch.save(model.state_dict(),
                           args.ckpt_dir / f'{best_acc}-1.pt')
                tqdm.write(f'saved model with acc: {best_acc}')

def main2(args: Namespace):
    with open(args.cache_dir / "vocab.pkl", "rb") as vocab_file:
        vocab: Vocab = pickle.load(vocab_file)

    intent_idx_path = args.cache_dir / "tag2idx.json"
    intent2idx: Dict[str, int] = json.loads(intent_idx_path.read_text())

    data_paths = {split: args.data_dir / f"{split}.json" for split in SPLITS}
    data = {
        split: json.loads(path.read_text())
        for split, path in data_paths.items()
    }
    datasets: Dict[str, SeqTagDataset] = {
        split: SeqTagDataset(split_data, vocab, intent2idx, args.max_len)
        for split, split_data in data.items()
    }
    data_loaders = {
        split: torch.utils.data.DataLoader(dataset=dataset,
                                           batch_size=args.batch_size,
                                           shuffle=True,
                                           collate_fn=dataset.collate_fn,
                                           num_workers=args.load_workers)
        for split, dataset in datasets.items()
    }

    embeddings = torch.load(args.cache_dir / "embeddings.pt")
    model = SlotTagger(embeddings, args.hidden_size, args.num_layers,
                       args.dropout, args.bidirectional,
                       datasets[TRAIN].num_classes,
                       args.max_len).to(args.device)

    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
    criterion = torch.nn.CrossEntropyLoss()

    epoch_pbar = trange(args.num_epoch, desc="Epoch")
    best_acc: float = 0
    for epoch in epoch_pbar:
        if best_acc >= 770:
            break
        model.train()
        train_loss: float = 0
        train_acc: float = 0
        for batch in data_loaders[TRAIN]:
            texts = torch.tensor(batch['tokens'], device=args.device)
            intents = torch.tensor(batch['tags'], device=args.device)

            optimizer.zero_grad()
            result = model(texts)
            loss = criterion(result.reshape(-1, datasets[TRAIN].num_classes), intents.reshape(-1))

            loss.backward()
            optimizer.step()

            train_acc += torch.sum(torch.all(torch.eq(result.argmax(2), intents),
                                             dim=1)).item()
            train_loss += loss.item()
        tqdm.write(
            f'[{epoch: 3d} / {args.num_epoch}] - train_loss: {train_loss: .5f}, train_acc: {train_acc / len(datasets[TRAIN]): .5f}'
        )

        eval_loss: float = 0
        eval_acc: float = 0
        model.eval()
        with torch.no_grad():
            for batch in data_loaders[DEV]:
                texts = torch.tensor(batch['tokens'], device=args.device)
                intents = torch.tensor(batch['tags'], device=args.device)

                result = model(texts)
                loss = criterion(result.reshape(-1, datasets[TRAIN].num_classes), intents.reshape(-1))

                eval_acc += torch.sum(
                    torch.all(torch.eq(result.argmax(2), intents), dim=1)).item()

                eval_loss += loss.item()
            tqdm.write(
                f'[{epoch: 3d} / {args.num_epoch}] - valid_loss: {eval_loss: .5f}, valid_acc: {eval_acc / len(datasets[DEV]): .5f}'
            )
            if eval_acc > best_acc:
                best_acc = eval_acc
                torch.save(model.state_dict(),
                           args.ckpt_dir / f'{best_acc}-2.pt')
                tqdm.write(f'saved model with acc: {best_acc}')

def parse_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument(
        "--data_dir",
        type=Path,
        help="Directory to the dataset.",
        default="./data/slot/",
    )
    parser.add_argument(
        "--cache_dir",
        type=Path,
        help="Directory to the preprocessed caches.",
        default="./cache/slot/",
    )
    parser.add_argument(
        "--ckpt_dir",
        type=Path,
        help="Directory to save the model file.",
        default="./ckpt/slot/",
    )

    # data
    parser.add_argument("--max_len", type=int, default=35)

    # model
    parser.add_argument("--hidden_size", type=int, default=1024)
    parser.add_argument("--num_layers", type=int, default=4)
    parser.add_argument("--dropout", type=float, default=0.4)
    parser.add_argument("--bidirectional", type=bool, default=True)

    # optimizer
    parser.add_argument("--lr", type=float, default=5e-5)

    # data loader
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--load_workers", type=int, default=0)

    # training
    parser.add_argument("--device",
                        type=torch.device,
                        help="cpu, cuda, cuda:0, cuda:1",
                        default="cuda:0")
    parser.add_argument("--num_epoch", type=int, default=5000)

    return parser.parse_args([])


if __name__ == "__main__":
    args = parse_args()
    args.ckpt_dir.mkdir(parents=True, exist_ok=True)
    main1(args)
    main2(args)
