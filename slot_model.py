from typing import Dict

import torch
from torch import nn


class SlotTagger(torch.nn.Module):
    def __init__(
        self,
        embeddings: torch.tensor,
        hidden_size: int,
        num_layers: int,
        dropout: float,
        bidirectional: bool,
        num_class: int,
        max_len: int,
    ) -> None:
        super().__init__()
        self.num_class = num_class
        self.embed = nn.Embedding.from_pretrained(embeddings, freeze=False)
        self.lstm = nn.LSTM(self.embed.embedding_dim,
                           hidden_size,
                           num_layers=num_layers,
                           dropout=dropout,
                           batch_first=True,
                           bidirectional=bidirectional)
        self.encoder_output_size = (2 if bidirectional else 1) * hidden_size
        self.classifier = nn.Sequential(
            nn.Dropout(dropout),
            nn.Linear(self.encoder_output_size, 1024),
            nn.LeakyReLU(),
            nn.Linear(1024, 1024),
            nn.LeakyReLU(),
            nn.Linear(1024, 1024),
            nn.LeakyReLU(),
            nn.Linear(1024, num_class),
        ) # yapf: disable

    def forward(self, batch) -> Dict[str, torch.Tensor]:
        embeded_batch = self.embed(batch)
        output, hidden = self.lstm(embeded_batch)
        classified = self.classifier(output)
        return classified
