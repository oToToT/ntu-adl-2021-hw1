import json
import pickle
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Dict

import torch
from tqdm import trange, tqdm

from intent_model import SeqClassifier
from dataset import SeqClsDataset
from utils import Vocab

torch.autograd.set_detect_anomaly(True)


TRAIN = "train"
DEV = "eval"
SPLITS = [TRAIN, DEV]


def main(args: Namespace):
    with open(args.cache_dir / "vocab.pkl", "rb") as vocab_file:
        vocab: Vocab = pickle.load(vocab_file)

    intent_idx_path = args.cache_dir / "intent2idx.json"
    intent2idx: Dict[str, int] = json.loads(intent_idx_path.read_text())

    data_paths = {split: args.data_dir / f"{split}.json" for split in SPLITS}
    data = {
        split: json.loads(path.read_text())
        for split, path in data_paths.items()
    }
    datasets: Dict[str, SeqClsDataset] = {
        split: SeqClsDataset(split_data, vocab, intent2idx, args.max_len)
        for split, split_data in data.items()
    }
    data_loaders = {
        split: torch.utils.data.DataLoader(dataset=dataset,
                                           batch_size=args.batch_size,
                                           shuffle=True,
                                           collate_fn=dataset.collate_fn,
                                           num_workers=args.load_workers)
        for split, dataset in datasets.items()
    }

    embeddings = torch.load(args.cache_dir / "embeddings.pt")
    model = SeqClassifier(embeddings, args.hidden_size, args.num_layers,
                          args.dropout, args.bidirectional,
                          datasets[TRAIN].num_classes,
                          args.max_len).to(args.device)

    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
    criterion = torch.nn.CrossEntropyLoss()

    epoch_pbar = trange(args.num_epoch, desc="Epoch")
    best_acc: float = 0
    for epoch in epoch_pbar:
        if best_acc >= 2790:
            break
        model.train()
        train_loss: float = 0
        train_acc: float = 0
        for batch in data_loaders[TRAIN]:
            texts = torch.tensor(batch['text'], device=args.device)
            intents = torch.tensor(batch['intent'], device=args.device)

            optimizer.zero_grad()
            result = model(texts)
            loss = criterion(result, intents)

            loss.backward()
            optimizer.step()

            train_loss += loss.item()
            train_acc += torch.sum(result.argmax(1) == intents).item()
        tqdm.write(
            f'[{epoch: 3d} / {args.num_epoch}] - train_loss: {train_loss: .5f}, train_acc: {train_acc / len(datasets[TRAIN]): .5f}'
        )

        eval_loss: float = 0
        eval_acc: float = 0
        model.eval()
        with torch.no_grad():
            for batch in data_loaders[DEV]:
                texts = torch.tensor(batch['text'], device=args.device)
                intents = torch.tensor(batch['intent'], device=args.device)

                result = model(texts)
                loss = criterion(result, intents)

                eval_acc += torch.sum(result.argmax(1) == intents).item()
                eval_loss += loss.item()
            tqdm.write(
                f'[{epoch: 3d} / {args.num_epoch}] - valid_loss: {eval_loss: .5f}, valid_acc: {eval_acc / len(datasets[DEV]): .5f}'
            )
            if eval_acc > best_acc:
                best_acc = eval_acc
                torch.save(model.state_dict(),
                           args.ckpt_dir / f'{best_acc}.pt')
                tqdm.write(f'saved model with acc: {best_acc}')
    
def parse_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument(
        "--data_dir",
        type=Path,
        help="Directory to the dataset.",
        default="./data/intent/",
    )
    parser.add_argument(
        "--cache_dir",
        type=Path,
        help="Directory to the preprocessed caches.",
        default="./cache/intent/",
    )
    parser.add_argument(
        "--ckpt_dir",
        type=Path,
        help="Directory to save the model file.",
        default="./ckpt/intent/",
    )

    # data
    parser.add_argument("--max_len", type=int, default=40)

    # model
    parser.add_argument("--hidden_size", type=int, default=1024)
    parser.add_argument("--num_layers", type=int, default=2)
    parser.add_argument("--dropout", type=float, default=0.1)
    parser.add_argument("--bidirectional", type=bool, default=False)

    # optimizer
    parser.add_argument("--lr", type=float, default=1e-3)

    # data loader
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--load_workers", type=int, default=0)

    # training
    parser.add_argument("--device",
                        type=torch.device,
                        help="cpu, cuda, cuda:0, cuda:1",
                        default="cuda:0")
    parser.add_argument("--num_epoch", type=int, default=5000)

    return parser.parse_args([])


if __name__ == "__main__":
    args = parse_args()
    args.ckpt_dir.mkdir(parents=True, exist_ok=True)
    main(args)
