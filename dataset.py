from typing import List, Dict

from torch.utils.data import Dataset

from utils import Vocab


class SeqClsDataset(Dataset):
    def __init__(
        self,
        data: List[Dict],
        vocab: Vocab,
        label_mapping: Dict[str, int],
        max_len: int,
    ):
        self.labeled = False
        if len(data) > 0:
            self.labeled = 'intent' in data[0]
        self.data = data
        for i in range(len(data)):
            data[i]['text'] = data[i]['text'].split()
        self.vocab = vocab
        self.label_mapping = label_mapping
        self._idx2label = {
            idx: intent
            for intent, idx in self.label_mapping.items()
        }
        self.max_len = max_len

    def __len__(self) -> int:
        return len(self.data)

    def __getitem__(self, index) -> Dict:
        instance = self.data[index]
        return instance

    @property
    def num_classes(self) -> int:
        return len(self.label_mapping)

    def collate_fn(self, samples: List[Dict]) -> Dict:
        result: Dict = {'text': [], 'id': []}
        if self.labeled:
            result['intent'] = []
        for sample in samples:
            result['text'].append(sample['text'])
            result['id'].append(sample['id'])
            if self.labeled:
                result['intent'].append(self.label2idx(sample['intent']))
        result['text'] = self.vocab.encode_batch(result['text'], self.max_len)
        return result

    def label2idx(self, label: str):
        return self.label_mapping[label]

    def idx2label(self, idx: int):
        return self._idx2label[idx]


class SeqTagDataset(Dataset):
    def __init__(
        self,
        data: List[Dict],
        vocab: Vocab,
        label_mapping: Dict[str, int],
        max_len: int,
    ):
        self.labeled = False
        if len(data) > 0:
            self.labeled = 'tags' in data[0]
        self.data = data
        self.vocab = vocab
        self.label_mapping = label_mapping
        self._idx2label = {
            idx: intent
            for intent, idx in self.label_mapping.items()
        }
        self.max_len = max_len

    def __len__(self) -> int:
        return len(self.data)

    def __getitem__(self, index) -> Dict:
        instance = self.data[index]
        return instance

    @property
    def num_classes(self) -> int:
        return len(self.label_mapping)

    def collate_fn(self, samples: List[Dict]) -> Dict:
        result: Dict = {'tokens': [], 'len': [], 'id': []}
        if self.labeled:
            result['tags'] = []
        for sample in samples:
            result['tokens'].append(sample['tokens'])
            result['id'].append(sample['id'])
            result['len'].append(len(sample['tokens']))
            if self.labeled:
                tags = [self.label2idx(label) for label in sample['tags']]
                result['tags'].append(tags[:self.max_len] +
                                      [self.label2idx('pad')] *
                                      max(0, self.max_len - len(tags)))
        result['tokens'] = self.vocab.encode_batch(result['tokens'],
                                                   self.max_len)
        return result

    def label2idx(self, label: str):
        return self.label_mapping[label]

    def idx2label(self, idx: int):
        return self._idx2label[idx]
