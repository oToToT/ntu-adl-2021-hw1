import json
import pickle
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Dict

import torch

from dataset import SeqTagDataset
from slot_model import SlotTagger
from utils import Vocab


def main(args):
    result_csv = open(args.pred_file, 'w', encoding='utf8')
    result_csv.write('id,tags\n')

    with open(args.cache_dir / "vocab.pkl", "rb") as f:
        vocab: Vocab = pickle.load(f)

    intent_idx_path = args.cache_dir / "tag2idx.json"
    intent2idx: Dict[str, int] = json.loads(intent_idx_path.read_text())

    data = json.loads(args.test_file.read_text())
    dataset = SeqTagDataset(data, vocab, intent2idx, args.max_len)
    data_loader = torch.utils.data.DataLoader(dataset=dataset,
                                              batch_size=args.batch_size,
                                              shuffle=False,
                                              collate_fn=dataset.collate_fn,
                                              num_workers=args.load_workers)

    embeddings = torch.load(args.cache_dir / "embeddings.pt")
    model1 = SlotTagger(embeddings, args.hidden_size, args.num_layers,
                       args.dropout, args.bidirectional, dataset.num_classes,
                       args.max_len).to(args.device)
    model1.load_state_dict(torch.load('./slot-models/771.pt'))
    model2 = SlotTagger(embeddings, args.hidden_size, args.num_layers,
                       args.dropout, args.bidirectional, dataset.num_classes,
                       args.max_len).to(args.device)
    model2.load_state_dict(torch.load('./slot-models/770.pt'))

    model1.eval()
    model2.eval()
    with torch.no_grad():
        for batch in data_loader:
            texts = torch.tensor(batch['tokens'], device=args.device)

            result = (model1(texts) + model2(texts)).argmax(2)
            for i, idx in enumerate(batch['id']):
                tags = [dataset.idx2label(r.item()) for r in result[i]]
                result_csv.write(
                    f'{idx},{" ".join(tags[:batch["len"][i]])}\n'
                )


def parse_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("--test_file",
                        type=Path,
                        help="Path to the test file.",
                        default='./data/slot/test.json')
    parser.add_argument(
        "--cache_dir",
        type=Path,
        help="Directory to the preprocessed caches.",
        default="./slot-models/",
    )
    parser.add_argument("--pred_file", type=Path, default="pred.slot.csv")

    # data
    parser.add_argument("--max_len", type=int, default=35)

    # model
    parser.add_argument("--hidden_size", type=int, default=1024)
    parser.add_argument("--num_layers", type=int, default=4)
    parser.add_argument("--dropout", type=float, default=0.4)
    parser.add_argument("--bidirectional", type=bool, default=True)

    # data loader
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--load_workers", type=int, default=0)

    parser.add_argument("--device",
                        type=torch.device,
                        help="cpu, cuda, cuda:0, cuda:1",
                        default="cuda:0")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    main(args)
    print("Done!")
