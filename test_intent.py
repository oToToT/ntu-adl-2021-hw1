import json
import pickle
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Dict

import torch

from dataset import SeqClsDataset
from intent_model import SeqClassifier
from utils import Vocab


def main(args):
    result_csv = open(args.pred_file, 'w', encoding='utf8')
    result_csv.write('id,intent\n')

    with open(args.cache_dir / "vocab.pkl", "rb") as f:
        vocab: Vocab = pickle.load(f)

    intent_idx_path = args.cache_dir / "intent2idx.json"
    intent2idx: Dict[str, int] = json.loads(intent_idx_path.read_text())

    data = json.loads(args.test_file.read_text())
    dataset = SeqClsDataset(data, vocab, intent2idx, args.max_len)
    data_loader = torch.utils.data.DataLoader(dataset=dataset,
                                              batch_size=args.batch_size,
                                              shuffle=False,
                                              collate_fn=dataset.collate_fn,
                                              num_workers=args.load_workers)

    embeddings = torch.load(args.cache_dir / "embeddings.pt")
    model = SeqClassifier(embeddings, args.hidden_size, args.num_layers,
                          args.dropout, args.bidirectional,
                          dataset.num_classes, args.max_len).to(args.device)
    model.load_state_dict(torch.load(args.ckpt_path))

    model.eval()
    with torch.no_grad():
        for batch in data_loader:
            texts = torch.tensor(batch['text'], device=args.device)

            result = model(texts).argmax(1)
            for i, idx in enumerate(batch['id']):
                result_csv.write(
                    f'{idx},{dataset.idx2label(result[i].item())}\n')


def parse_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("--test_file",
                        type=Path,
                        help="Path to the test file.",
                        default="./data/intent/test.json")
    parser.add_argument(
        "--cache_dir",
        type=Path,
        help="Directory to the preprocessed caches.",
        default="./intent-models/",
    )
    parser.add_argument("--ckpt_path",
                        type=Path,
                        help="Path to model checkpoint.",
                        default='./intent-models/2791.pt')
    parser.add_argument("--pred_file", type=Path, default="pred.intent.csv")

    # data
    parser.add_argument("--max_len", type=int, default=40)

    # model
    parser.add_argument("--hidden_size", type=int, default=1024)
    parser.add_argument("--num_layers", type=int, default=2)
    parser.add_argument("--dropout", type=float, default=0.1)
    parser.add_argument("--bidirectional", type=bool, default=False)

    # data loader
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--load_workers", type=int, default=0)

    parser.add_argument("--device",
                        type=torch.device,
                        help="cpu, cuda, cuda:0, cuda:1",
                        default="cuda:0")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    main(args)
    print("Done!")
