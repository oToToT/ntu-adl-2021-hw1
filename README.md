# Homework 1 ADL NTU 2021 Spring

## Environment
```shell
# If you have conda, we recommend you to build a conda environment called "adl"
make
# otherwise
pip install -r requirements.txt
```

## Preprocessing
```shell
# To preprocess intent detectiona and slot tagging datasets
bash preprocess.sh
```

## Intent detection
```shell
python3.8 train_intent.py
```

## Slot Tagging
```shell
python3.8 train_slot.py
```

The default argument of these two models should works fine, so simply run them is OK.
You will get your model in `ckpt/intent/` and `ckpt/slot/` for intent detection and slot tagging respectively.
The saved model will be named by their accuracy (for example, `ckpt/intent/200.pt`), so you might want to pick the model with highest accuracy.

You should put your data in `data/intent/` and `data/slot/` as provided.
